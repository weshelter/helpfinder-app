import React from 'react';
import {lifecycle, compose, withState} from 'recompose';
import {View} from 'react-native';
import {MapView} from 'expo';
import {equals} from 'ramda';
import DefaultView from '../../views/defaultView';
import MapCalloutView from './mapCalloutView';

const PADDING = 40;

const HFMapView = props => (
  <View style={{flex: 1}}>
    {props.markers.length ? <Map {...props} /> : <DefaultView/>}
  </View>
);

const Map = props => (
  <MapView
    style={{flex: 1}}
    ref={props.setMapRef}
    showsUserLocation
    onLayout={() => focusMap(props)}
  >
    {props.markers.map(marker =>
      <MapView.Marker key={marker.id} coordinate={marker.coordinate} >
        <MapView.Callout tooltip style={{width: 300, height: 70}} onPress={() => props.onMarkerPress(marker.id)}>
          <MapCalloutView title={marker.title}/>
        </MapView.Callout>
      </MapView.Marker>
    )}
  </MapView>
);

const focusMap = ({mapRef, markers, mapCenter}, animated = false) => {
  if (mapRef && markers.length) {
    const closestMarker = markers[0].coordinate;

    mapRef.fitToCoordinates([mapCenter, closestMarker], {
      edgePadding: {top: PADDING, right: PADDING, bottom: PADDING, left: PADDING},
      animated,
    });
  }
};

export default compose(
  withState('mapRef', 'setMapRef'),
  lifecycle({
    componentWillReceiveProps(nextProps) {
      if (!equals(nextProps.markers, this.props.markers)) {
        setTimeout(() => focusMap(nextProps, true), 250);
      }

    }
  })
)(HFMapView);
