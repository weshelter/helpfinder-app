import React from 'react';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {selectService} from '../../actions/services';
import {getServiceMarkers, getCurrentLocation} from '../../selectors';
import CategoryPicker from '../categoryPicker/categoryButton';
import MapView from './mapView';

const MapPage = props => (
  <View style={StyleSheet.absoluteFill}>
    <MapView
      mapCenter={props.currentLocation}
      markers={props.markers}
      onMarkerPress={props.selectService} />
    <CategoryPicker />
  </View>
);

const mapStateToProps = state => ({
  markers: getServiceMarkers(state),
  currentLocation: getCurrentLocation(state),
});

export default connect(mapStateToProps, {selectService})(MapPage);
