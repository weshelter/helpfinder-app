import React from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

const MapCalloutView = props => (
  <View style={[styles.callout]}>
    <Text>{props.title}</Text>
    <View>
      <Text style={{color: 'blue', fontSize: 18}}>
        >
      </Text>
    </View>
  </View>
);
export default MapCalloutView;

const styles = StyleSheet.create({
  callout: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(255,255,255,0.9)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  }
});
