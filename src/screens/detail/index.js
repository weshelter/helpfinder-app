import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import {selectSubCategory} from '../../actions/categories';
import {getSelectedService} from '../../selectors';
import * as Links from '../../views/links';

const DetailPage = props => (
  <View style={StyleSheet.absoluteFill}>
    <ScrollView>
      <View style={styles.header}>
        <Text style={styles.title}>{props.name}</Text>
        <Text style={{marginTop: 8}}>{props.information}</Text>
        <View style={{paddingVertical: 15, flexDirection: 'row', alignItems: 'center'}}>
          <View style={styles.line}/>
        </View>
        <Text>{props.requirements}</Text>
      </View>
      <View style={styles.footer}>
        {
          Links.addressContainer(formattedAddress(
            props.address,
            props.administrative_area,
            props.city,
            props.zip))
        }
        {Links.phoneContainer(props.phone_number_1)}
        {Links.websiteContainer(props.web_url)}
        <View style={styles.tagList}>
          {props.categories.map((category, index) => (
            <TouchableOpacity key={index} onPress={() => props.selectSubCategory(category.id)}>
              <Text style={styles.tag}>{category.name}</Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    </ScrollView>
  </View>
);
DetailPage.route = {navigationBar: {renderLeft: null, renderRight: null}};

const formattedAddress = (address, administrative_area, city, zip) =>
  address
    ? `${address}\n${administrative_area ? administrative_area + ', ' : ''}${city} ${zip || ''}`
    : '';

export default connect(state => ({...getSelectedService(state)}), {selectSubCategory})(DetailPage);

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'white',
    padding: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#b3b3b3'
  },
  line: {
    flex: 1,
    height: 1,
    backgroundColor: '#b3b3b3'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#3c3c3c'
  },
  footer: {
    backgroundColor: '#efefef',
    flex: 1
  },
  tagList: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: 20
  },
  tag: {
    backgroundColor: 'blue',
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
    marginHorizontal: 5
  }
});
