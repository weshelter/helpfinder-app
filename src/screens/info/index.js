import React from 'react';
import {List, ListItem} from 'react-native-elements';
import {openEmail, openWebsite} from '../../views/links';
import {rowColor} from '../../views/styles';

const InfoPage = () =>
  <List containerStyle={{marginTop: 0}}>
    <ListItem
      key={0}
      title={'About HelpFinder'}
      containerStyle={rowColor(0)}
      rightIcon={{color: 'blue'}}
      onPress={openWebsite('https://helpfinder.nyc/about/')}
    />
    <ListItem
      key={1}
      title={'Send Feedback'}
      containerStyle={rowColor(1)}
      rightIcon={{color: 'blue'}}
      onPress={openEmail('info@helpfinder.nyc')}
    />
    <ListItem
      key={2}
      title={'Submit a New Service'}
      containerStyle={rowColor(2)}
      rightIcon={{color: 'blue'}}
      onPress={openEmail('info@helpfinder.nyc')}
    />
    <ListItem
      key={3}
      title={'Terms and Conditions'}
      containerStyle={rowColor(3)}
      rightIcon={{color: 'blue'}}
      onPress={openWebsite('https://helpfinder.nyc/terms')}
    />
  </List>;
InfoPage.route = {navigationBar: {renderLeft: null, renderRight: null}};

export default InfoPage;
