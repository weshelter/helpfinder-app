import React from 'react';
import {ScrollView} from 'react-native';
import {List, ListItem} from 'react-native-elements';
import {rowColor} from '../../views/styles';

const ListView = props =>
  <ScrollView>
    <List containerStyle={{marginTop: 0}}>
      {
        props.rows.map((row, i) =>
          <ListItem
            key={row.id}
            title={trim(row.title, 35)}
            subtitle={row.message || null}
            containerStyle={rowColor(i)}
            onPress={props.onPress.bind(null, row.id)}
          />
        )
      }
    </List>
  </ScrollView>;

const trim = (str, max) =>
  (str.length > max)
    ? str.substring(0, max - 3) + '...'
    : str;

export default ListView;
