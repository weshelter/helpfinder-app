import React from 'react';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {selectService} from '../../actions/services';
import {getServiceRows, getSubCategories} from '../../selectors';
import DefaultView from '../../views/defaultView';
import CategoryButton from '../categoryPicker/categoryButton';
import ListView from './listView';

const ListPage = props => (
  <View style={StyleSheet.absoluteFill}>
    {props.rows.length ? <ListView rows={props.rows} onPress={props.selectService} /> : <DefaultView />}
    <CategoryButton/>
  </View>
);

const mapStateToProps = state => ({
  rows: getServiceRows(state),
  categories: getSubCategories(state)
});

export default connect(mapStateToProps, {selectService})(ListPage);
