import React from 'react';
import {Button} from 'react-native-elements';
import {connect} from 'react-redux';
import * as selectors from '../../selectors';
import {openCategoryPicker} from '../../actions/categories';

const DEFAULT_TEXT = 'Select a category';

const PickerButton = props =>
  <Button
    buttonStyle={{height: 70}}
    containerViewStyle={{marginLeft: 0, marginRight: 0}}
    backgroundColor='#1db264'
    fontSize={24}
    title={getTitle(props.selectedSubCategory, props.selectedSuperCategory)}
    onPress={props.openCategoryPicker}
  />;

const getTitle = (selectedSubCategory, selectedSuperCategory) =>
  selectedSubCategory
    ? selectedSubCategory.name
    : selectedSuperCategory
      ? selectedSuperCategory.name
      : DEFAULT_TEXT;

const mapStateToProps = state => ({
  selectedSubCategory: selectors.getSelectedSubCategory(state),
  selectedSuperCategory: selectors.getSelectedSuperCategory(state)
});

export default connect(mapStateToProps, {openCategoryPicker})(PickerButton);
