import React from 'react';
import {compose, lifecycle} from 'recompose';
import {View, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import {List, ListItem} from 'react-native-elements';
import {ALL_CATEGORY} from '../../models/category';
import {fetchSuperCategories, selectSuperCategory, selectSubCategory} from '../../actions/categories';
import {getSuperCategories, getSelectedSuperCategory, getSubCategories, getSelectedSubCategory} from '../../selectors';

const SELECTED_ICON = {name: 'done', color: 'gray'}; // react-native-elements vector icon

export const SuperCategoryPicker = compose(
  connect(
    state => ({
      superCategories: getSuperCategories(state),
      selectedSuperCategory: getSelectedSuperCategory(state),
    }),
    {fetchSuperCategories, selectSuperCategory}
  ),
  lifecycle({
    componentDidMount() {
      this.props.fetchSuperCategories();
    }
  })
)(props =>
  <CategoryPicker
    categories={props.superCategories}
    selectCategory={props.selectSuperCategory}
    selectedCategory={props.selectedSuperCategory}
  />
);
SuperCategoryPicker.route = {navigationBar: {renderLeft: null, renderRight: null}};

export const SubCategoryPicker = connect(
  state => ({
    subCategories: getSubCategories(state),
    selectedSuperCategory: getSelectedSuperCategory(state),
    selectedSubCategory: getSelectedSubCategory(state),
  }),
  {selectSubCategory}
)(props =>
 <CategoryPicker
    categories={[ALL_CATEGORY(props.selectedSuperCategory), ...props.subCategories]}
    selectCategory={props.selectSubCategory}
    selectedCategory={props.selectedSubCategory}
  />
);
SubCategoryPicker.route = {navigationBar: {renderLeft: null, renderRight: null}};

const CategoryPicker = props => props.categories.length ? (
  <View>
    <ScrollView>
      <List containerStyle={{marginTop: 0}}>
        {props.categories.map(category =>
          <ListItem
            key={category.id}
            title={category.name}
            leftIcon={getIcon(props.selectedSubCategory, category)}
            onPress={() => props.selectCategory(category.id)}
            hideChevron
          />
        )}
      </List>
    </ScrollView>
  </View>
) : null;

const getIcon = (selectedCategory, category) =>
  (selectedCategory && category.id === selectedCategory.id) ? SELECTED_ICON : null;
