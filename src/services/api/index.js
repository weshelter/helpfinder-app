/*global fetch*/
import {toService} from '../../models/service';
import {toCategory} from '../../models/category';

//const baseUrl = 'http://localhost:3000/api/v1';
//const baseUrl = 'https://helpfinder-stage.herokuapp.com/api/v1';
const baseUrl = 'https://helpfinder-prod.herokuapp.com/api/v1';

export const getSuperCategories = () =>
  fetch(`${baseUrl}/tags`)
    .then(response => response.json())
    .then(response => response.data.map(toCategory));

export const getSubCategoriesBySuperCategory = (id) =>
  fetch(`${baseUrl}/tags/${id}/subtags`)
    .then(response => response.json())
    .then(response => response.data.map(toCategory));

export const getServicesByCategory = (id, {lat, lng}) =>
  fetch(`${baseUrl}/tags/${id}/services?lat=${lat}&lng=${lng}`)
    .then(response => response.json())
    .then(response => response.data.map(toService));
