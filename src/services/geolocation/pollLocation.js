import {Location, Permissions} from 'expo';

export async function pollLocation(callback) {
  Location.setApiKey('AIzaSyD4zkU3Eeskbril-MstTTXvm1K45oEUvEA');
  const providerStatus = await Location.getProviderStatusAsync();
  console.log('got providerStatus', providerStatus);
  if (!providerStatus.locationServicesEnabled) {
    throw new Error('Location services disabled');
  }
  const {status} = await Permissions.askAsync(Permissions.LOCATION);
  console.log('got permissions', status);
  if (status === 'granted') {
    Location.watchPositionAsync({
      enableHighAccuracy: false,
    }, callback);
  } else {
    throw new Error('Location permission not granted');
  }
}
