import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import sharedStyles from './styles';

const DefaultView = () =>
  <View style={[sharedStyles.flex, styles.container]}>
    <Text style={[sharedStyles.heading, sharedStyles.brand, styles.text]}>
      {`Welcome to HelpFinder!\nFind helpful services near you.\nSelect a category below to get started.`}
    </Text>
  </View>;

const styles = StyleSheet.create({
  text: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#162a3f',
    flexDirection: 'column',
    justifyContent: 'center',
  }
});

export default DefaultView;
