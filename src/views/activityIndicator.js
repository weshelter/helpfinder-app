import React from 'react';
import {StyleSheet, ActivityIndicator, View, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import {getIsFetching} from '../selectors';

const windowDimensions = Dimensions.get('window');

const HFActivityIndicator = ({isFetching}) =>
  isFetching &&
    <View style={styles.indicatorContainer}>
      <ActivityIndicator
        animating={isFetching}
        style={styles.indicator}
        size='large'
        color='grey'
      />
    </View>;

const styles = StyleSheet.create({
    indicatorContainer: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        left: 0,
        height: windowDimensions.height,
        width: windowDimensions.width
    },
    indicator: {
        height: 80
    }
});

const mapStateToProps = state => ({
    isFetching: getIsFetching(state)
});

export default connect(mapStateToProps)(HFActivityIndicator);
