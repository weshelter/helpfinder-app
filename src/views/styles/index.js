import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  brand: {
    fontSize: 18
  },
  heading: {
    alignSelf: 'center',
    marginBottom: 10
  },
  flex: {
    flex: 1
  },
  row: {
    padding: 20,
    height: 87,
    flexDirection: 'row',
    alignItems: 'center'
  },
  rowContent: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between'
  }
});

export const rowColor = rowIndex => ({
    backgroundColor: (rowIndex % 2 === 1) ? '#efefef' : 'white'
});
