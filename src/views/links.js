import React from 'react';
import {View, TouchableOpacity, Linking, Text, StyleSheet, Platform} from 'react-native';

const stripNonNumerics = (str) => str.replace(/\D/g, '');
const stripProtocolFromURL = (url) => url.replace(/.*?:(\/\/)?/g, "");

export const openWebsite = (url) => () => Linking.openURL(`http://${stripProtocolFromURL(url)}`);
export const openEmail = (url) => () => Linking.openURL(`mailto:${url}`);
const openPhoneTo = (phoneNumber) => () => Linking.openURL(`tel:${stripNonNumerics(phoneNumber)}`);
const openMapTo = (address) => () =>
  Linking.openURL((Platform.OS === 'ios')
    ? `http://maps.apple.com/?q=${address}`
    : `geo:0,0?q=${encodeURIComponent(address)}`);

const linkContainer = (title, content, openFunc, formatFunc) =>
  content &&
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <TouchableOpacity onPress={openFunc(content)}>
        <Text style={styles.content}>{formatFunc ? formatFunc(content) : content}</Text>
      </TouchableOpacity>
    </View>;

export const websiteContainer = (url) => linkContainer('Website', url, openWebsite);
export const phoneContainer = (phoneNumber) => linkContainer('Phone', phoneNumber, openPhoneTo);
export const addressContainer = (address) => linkContainer('Address', address, openMapTo);

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#3c3c3c'
  },
  content: {
    marginTop: 10,
    color: 'blue'
  },
  container: {
    padding: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#b3b3b3'
  }
});
