import {createRouter, NavigationContext} from '@expo/ex-navigation';
import Store from './store';
import MapPage from './screens/map';
import ListPage from './screens/list';
import DetailPage from './screens/detail';
import InfoPage from './screens/info';
import {SubCategoryPicker, SuperCategoryPicker} from './screens/categoryPicker';

export const Routes = {
  MAP_PAGE: 'map',
  LIST_PAGE: 'list',
  DETAIL_PAGE: 'detail',
  INFO_PAGE: 'info',
  SUPERCATEGORY_PICKER: 'superCategoryPicker',
  SUBCATEGORY_PICKER: 'subCategoryPicker',
};

export const Router = createRouter(() => ({
  [Routes.MAP_PAGE]: () => MapPage,
  [Routes.LIST_PAGE]: () => ListPage,
  [Routes.DETAIL_PAGE]: () => DetailPage,
  [Routes.INFO_PAGE]: () => InfoPage,
  [Routes.SUPERCATEGORY_PICKER]: () => SuperCategoryPicker,
  [Routes.SUBCATEGORY_PICKER]: () => SubCategoryPicker,
}));

export const navigationContext = new NavigationContext({
  router: Router,
  store: Store,
});
