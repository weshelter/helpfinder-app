import React from 'react';
import {View, StyleSheet} from 'react-native';
import {NavigationProvider, StackNavigation} from '@expo/ex-navigation';
import {lifecycle, compose} from 'recompose';
import {Provider, connect} from 'react-redux';
import Store from './store';
import {navigationContext, Routes} from './routes';
import {updateLocation} from './actions/location';
import {getNavBar} from './navigation/navBar';
import {pollLocation} from './services/geolocation/pollLocation';
import ActivityIndicator from './views/activityIndicator';

const enhancer = compose(
  connect(null, {updateLocation}),
  lifecycle({
    componentDidMount() {
      pollLocation(this.props.updateLocation);
    },
  })
);

const AppShell = enhancer(() => (
  <View style={StyleSheet.absoluteFill}>
    <StackNavigation id='root' initialRoute={Routes.MAP_PAGE}
      defaultRouteConfig={getNavBar()}
    />
    <ActivityIndicator/>
  </View>
));

export default () =>
  <Provider store={Store}>
    <NavigationProvider context={navigationContext}>
      <AppShell />
    </NavigationProvider>
  </Provider>;
