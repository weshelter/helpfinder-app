export const toService = (apiService) => ({
  id: apiService.id,
  categoryIds: getTagIdsFromService(apiService),
  ...apiService.attributes
});

export const getServiceSubtitle = service => {
  if(service.distance === null)
    return service.phone_number_1;
  else {
    const distance = Math.round(service.distance * 10) / 10;
    return `${distance} miles`;
  }
};

const getTagIdsFromService = (apiService) =>
  apiService.relationships.tags.data.map(tag => tag.id);

