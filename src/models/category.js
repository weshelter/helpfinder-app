export const toCategory = (apiCategory) => ({
  id: apiCategory.id,
  ...apiCategory.attributes
});

export const ALL_CATEGORY = (superCategory) => ({id: `${superCategory.id}-ALL`, name: `${superCategory.name} All`});
