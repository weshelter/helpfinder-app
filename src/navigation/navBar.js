import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native'
import {withNavigation} from '@expo/ex-navigation';
import {Routes, Router} from '../routes';

export const getNavBar = () => ({
  navigationBar: {
    backgroundColor: '#102339',
    renderLeft: () => <InfoButton />,
    renderTitle: () => <Title />,
    renderRight: route => <NavViewButton route={route} />,
  }
});

const Title = () => (
  <View style={styles.navItemContainer}>
    <Text style={styles.titleGreen}>help</Text>
    <Text style={styles.titleBlue}>finder</Text>
  </View>
);

const InfoButton = () => <NavButton newRoute={Routes.INFO_PAGE} text='Info' />;

const NavViewButton = ({route = {}}) => {
  const text = route.routeName === Routes.MAP_PAGE ? 'List' : 'Map';
  const newRoute = route.routeName === Routes.MAP_PAGE ? Routes.LIST_PAGE : Routes.MAP_PAGE;
  return <NavButton newRoute={newRoute} text={text} replace/>;
};

const NavButton = withNavigation(props => (
  <View style={[styles.navItemContainer, {marginHorizontal: 10}]}>
    <TouchableOpacity onPress={() => props.replace
      ? props.navigator.replace(Router.getRoute(props.newRoute))
      : props.navigator.push(Router.getRoute(props.newRoute))
    }>
      <Text style={styles.text}>{props.text}</Text>
    </TouchableOpacity>
  </View>
));

const styles = StyleSheet.create({
  content: {
    ...StyleSheet.absoluteFillObject
  },
  navItemContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    color: '#70aa5b'
  },
  titleBlue: {
    color: '#176dfa',
    fontWeight: 'bold',
    fontSize: 18
  },
  titleGreen: {
      color: '#70aa5b',
      fontWeight: 'bold',
      fontSize: 18
  }
});
