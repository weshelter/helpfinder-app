import * as categories from './categories';
import * as services from './services';
import {updateLocation} from './location';

export default {
  ...categories,
  ...services,
  updateLocation,
};
