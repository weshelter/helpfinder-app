import {NavigationActions} from '@expo/ex-navigation';
import * as api from '../services/api';
import {actionTypes} from '../reducers/services';
import {Router, Routes} from '../routes';
import {getCurrentLocation} from '../selectors';

export const fetchServicesByCategory = category => (dispatch, getState) => {
  dispatch({type: actionTypes.SERVICES_REQUEST});
  const {latitude, longitude} = getCurrentLocation(getState());
  api.getServicesByCategory(category, {lat: latitude, lng: longitude})
    .then(services =>
      dispatch({
        type: actionTypes.SERVICES_SUCCESS,
        services,
      })
    );
};

export const selectService = service => (dispatch, getState) => {
  dispatch({
    type: actionTypes.SERVICES_SELECT,
    service,
  });
  const navigatorUID = getState().navigation.currentNavigatorUID;
  dispatch(NavigationActions.push(navigatorUID, Router.getRoute(Routes.DETAIL_PAGE)));
};

export const deselectService = () => ({
  type: actionTypes.SERVICES_SELECT,
  service: null,
});
