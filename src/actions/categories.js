import {NavigationActions} from '@expo/ex-navigation';
import * as api from '../services/api';
import {actionTypes} from '../reducers/categories';
import {Router, Routes} from '../routes';
import {fetchServicesByCategory} from './services';

export const selectSuperCategory = category => (dispatch, getState) => {
  dispatch({
    type: actionTypes.SUPERCATEGORIES_SELECT,
    category,
  });
  dispatch(fetchSubCategoriesBySuperCategory(category));
  const navigatorUID = getState().navigation.currentNavigatorUID;
  dispatch(NavigationActions.push(navigatorUID, Router.getRoute(Routes.SUBCATEGORY_PICKER)));
};

export const selectSubCategory = category => (dispatch, getState) => {
  dispatch({
    type: actionTypes.SUBCATEGORIES_SELECT,
    category,
  });
  dispatch(fetchServicesByCategory(category));
  const navigatorUID = getState().navigation.currentNavigatorUID;
  dispatch(NavigationActions.pop(navigatorUID));
  dispatch(NavigationActions.pop(navigatorUID));
};

export const fetchSuperCategories = () => dispatch => {
  dispatch({type: actionTypes.CATEGORIES_REQUEST});
  api.getSuperCategories()
    .then(categories =>
      dispatch({
        type: actionTypes.SUPERCATEGORIES_SUCCESS,
        categories,
      })
    );
};

export const fetchSubCategoriesBySuperCategory = category => dispatch => {
  dispatch({type: actionTypes.CATEGORIES_REQUEST});
  api.getSubCategoriesBySuperCategory(category)
    .then(categories =>
      dispatch({
        type: actionTypes.SUBCATEGORIES_SUCCESS,
        categories,
      })
    );
};

export const openCategoryPicker = () => (dispatch, getState) => {
  const navigatorUID = getState().navigation.currentNavigatorUID;
  dispatch(NavigationActions.push(navigatorUID, Router.getRoute(Routes.SUPERCATEGORY_PICKER)));
};
