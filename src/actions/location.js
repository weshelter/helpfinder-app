import {actionTypes} from '../reducers/location';

export const updateLocation = ({coords}) => ({
  type: actionTypes.LOCATION_UPDATE,
  latitude: coords.latitude,
  longitude: coords.longitude
});
