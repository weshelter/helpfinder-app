export const actionTypes = {
  CATEGORIES_REQUEST: 'CATEGORIES_REQUEST',
  SUPERCATEGORIES_SUCCESS: 'SUPERCATEGORIES_SUCCESS',
  SUPERCATEGORIES_SELECT: 'SUPERCATEGORIES_SELECT',
  SUBCATEGORIES_SUCCESS: 'SUBCATEGORIES_SUCCESS',
  SUBCATEGORIES_SELECT: 'SUBCATEGORIES_SELECT',
};

const initialState = {
  isFetching: false,
  isErrored: false,
  superCategories: [],
  subCategories: [],
  selectedSuperCategory: null,
  selectedSubCategory: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CATEGORIES_REQUEST:
      return {
        ...state,
        isFetching: true,
        isErrored: false,
      };

    case actionTypes.SUPERCATEGORIES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isErrored: false,
        superCategories: action.categories,
      };

    case actionTypes.SUBCATEGORIES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isErrored: false,
        subCategories: action.categories,
      };

    case actionTypes.SUPERCATEGORIES_SELECT:
      return {
        ...state,
        selectedSuperCategory: action.category,
      };

    case actionTypes.SUBCATEGORIES_SELECT:
      return {
        ...state,
        selectedSubCategory: action.category,
      };
    default:
      return state;
  }
};
