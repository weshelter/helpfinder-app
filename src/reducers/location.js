export const actionTypes = {
  LOCATION_UPDATE: 'LOCATION_UPDATE',
};

const initialState = {
  latitude: 40.7528,
  longitude: -73.976522,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOCATION_UPDATE:
      return {
        ...state,
        latitude: action.latitude,
        longitude: action.longitude,
      };
    default:
      return state;
  }
}
