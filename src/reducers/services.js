export const actionTypes = {
  SERVICES_REQUEST: 'SERVICES_REQUEST',
  SERVICES_SUCCESS: 'SERVICES_SUCCESS',
  SERVICES_SELECT: 'SERVICES_SELECT',
  SERVICES_DESELECT: 'SERVICES_DESELECT',
};

const initialState = {
  isFetching: false,
  isErrored: false,
  allServices: [],
  selectedService: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SERVICES_REQUEST:
      return {
        ...state,
        isFetching: true,
        isErrored: false,
      };

    case actionTypes.SERVICES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isErrored: false,
        allServices: action.services,
      };

    case actionTypes.SERVICES_SELECT:
      return {
        ...state,
        selectedService: action.service,
      };

    case actionTypes.SERVICES_DESELECT:
      return {
        ...state,
        selectedService: null,
      };

    default:
      return state;
  }
}
