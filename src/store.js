import {applyMiddleware, createStore, combineReducers} from 'redux';
import {createNavigationEnabledStore, NavigationReducer} from '@expo/ex-navigation';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import categories from './reducers/categories';
import services from './reducers/services';
import location from './reducers/location';

const reducer = combineReducers({
  navigation: NavigationReducer,
  categories,
  services,
  location,
});

let middleware = [thunk];

/* if debug */ middleware.push(logger);

export default createNavigationEnabledStore({
  createStore,
  navigationStateKey: 'navigation',
})(reducer, applyMiddleware(...middleware));
