import {createSelector} from 'reselect';
import {getServiceSubtitle} from './models/service';

export const getServices = state => state.services.allServices;
export const getSuperCategories = state => state.categories.superCategories;
export const getSubCategories = state => state.categories.subCategories;
const getSelectedServiceId = state => state.services.selectedService;
const getSelectedSuperCategoryId = state => state.categories.selectedSuperCategory;
const getSelectedSubCategoryId = state => state.categories.selectedSubCategory;
export const getCurrentLocation = state => state.location;

export const getSelectedSuperCategory = createSelector(
  getSuperCategories,
  getSelectedSuperCategoryId,
  (superCategories, selectedSuperCategoryId) => superCategories.find(category => category.id === selectedSuperCategoryId)
);

export const getSelectedSubCategory = createSelector(
  getSubCategories,
  getSelectedSubCategoryId,
  (subCategories, selectedSubCategoryId) => subCategories.find(category => category.id === selectedSubCategoryId)
);

export const getSelectedService = createSelector(
  getServices,
  getSubCategories,
  getSelectedServiceId,
  (services, allSubCategories, selectedServiceId) => {
    const service = services.find(service => service.id === selectedServiceId);
    const categories = allSubCategories.filter(category => service.categoryIds.includes(category.id));
    return {
      ...service,
      categories,
    };
  }
);

export const getServiceMarkers = createSelector(
  getServices,
  services => services
    .filter(service => service.distance) // Filter out non-locations (remove for call centers)
    .map(service => ({
      coordinate: {
        latitude: service.latitude,
        longitude: service.longitude,
      },
      id: service.id,
      title: service.name,
    }))
);

export const getServiceRows = createSelector(
  getServices,
  services => services
    .map(service => ({
      id: service.id,
      title: service.name,
      message: getServiceSubtitle(service),
    }))
);

export const getIsFetching = state => state.services.isFetching || state.categories.isFetching;
